﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CAALPHUILib
{
    /// <summary>
    /// Interaction logic for CSTile.xaml
    /// </summary>
    public partial class CSTile : UserControl
    {
        public CSTile()
        {
            InitializeComponent();

            DataContext = this;

            this.delBtn.Visibility = Visibility.Collapsed;
            this.rightBtn.Visibility = Visibility.Collapsed;
            this.leftBtn.Visibility = Visibility.Collapsed;

        }

        static FrameworkPropertyMetadata propertymetadata =
            new FrameworkPropertyMetadata("",
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
            FrameworkPropertyMetadataOptions.Journal, new
            PropertyChangedCallback(MainTitle_PropertyChanged),
            new CoerceValueCallback(MainTitle_CoerceValue),
            false, UpdateSourceTrigger.PropertyChanged);

        static FrameworkPropertyMetadata propertymetadataImg =
            new FrameworkPropertyMetadata("",
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
            FrameworkPropertyMetadataOptions.Journal, new
            PropertyChangedCallback(IconSource_PropertyChanged),
            new CoerceValueCallback(IconSource_CoerceValue),
            false, UpdateSourceTrigger.PropertyChanged);

        public static readonly DependencyProperty MainTitleProperty = DependencyProperty.Register("MainTitle", typeof(String), typeof(CSTile), propertymetadata);
        public static readonly DependencyProperty IconSourceProperty = DependencyProperty.Register("IconSource", typeof(String), typeof(CSTile), propertymetadataImg);

        private static void MainTitle_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
        }
        private static void IconSource_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
        }


        private static object MainTitle_CoerceValue(DependencyObject dobj, object Value)
        {
            return Value;
        }

        private static object IconSource_CoerceValue(DependencyObject dobj, object Value)
        {
            return Value;
        }

        public String MainTitle
        {
            get
            {
                return (String)this.GetValue(MainTitleProperty);
            }
            set
            {
                this.SetValue(MainTitleProperty, value);
            }
        }


        public static void SetMainTitle(DependencyObject dependencyObject, String value)
        {
            dependencyObject.SetValue(MainTitleProperty, value);
        }

        public static String GetMainTitle(DependencyObject dependencyObject)
        {
            return (String)dependencyObject.GetValue(MainTitleProperty);
        }

        public String IconSource
        {
            get
            {
                return (String)this.GetValue(IconSourceProperty);
            }
            set
            {
                this.SetValue(IconSourceProperty, value);

                this.appIcon.Source = new BitmapImage(new Uri(value, UriKind.Relative));
            }
        }


        public static void SetIconSource(DependencyObject dependencyObject, String value)
        {
            dependencyObject.SetValue(IconSourceProperty, value);
        }

        public static String GetIconSource(DependencyObject dependencyObject)
        {
            return (String)dependencyObject.GetValue(IconSourceProperty);
        }

        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            this.Background = (Brush)bc.ConvertFrom("#FF3DBFD9");
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            this.Background = (Brush)bc.ConvertFrom("#803DBFD9");
        }

        private void editCanvas_MouseEnter(object sender, MouseEventArgs e)
        {
            this.delBtn.Visibility = Visibility.Visible;
            this.rightBtn.Visibility = Visibility.Visible;
            this.leftBtn.Visibility = Visibility.Visible;



        }

        private void editCanvas_MouseLeave(object sender, MouseEventArgs e)
        {
            this.delBtn.Visibility = Visibility.Collapsed;
            this.rightBtn.Visibility = Visibility.Collapsed;
            this.leftBtn.Visibility = Visibility.Collapsed;


        }

        private void delBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            delBtn.Opacity = 0.6;
        }

        private void delBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            delBtn.Opacity = 1.0;
        }

        private void rightBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            rightBtn.Opacity = 0.6;
        }

        private void rightBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            rightBtn.Opacity = 1.0;
        }

        private void leftBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            leftBtn.Opacity = 0.6;
        }

        private void leftBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            leftBtn.Opacity = 1.0;
        }


    }
}
