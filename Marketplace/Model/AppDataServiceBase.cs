using System;
using System.Collections.Specialized;
using System.Net;
using caalhp.Core.Utils.Helpers;

namespace Marketplace.Model
{
    public class AppDataServiceBase
    {
        protected readonly Uri BaseAddress = new Uri("https://carestoremarket.azurewebsites.net/");
        //protected WebClient WebClient;

        protected void AuthorizeCaalhp(CookieWebClient webClient)
        {
            try
            {
                var relativeUri = "api/caalhp/authorize/" + CaalhpConfigHelper.GetCaalhpId();
                var authUri = new Uri(BaseAddress, relativeUri);
                var nameValues = new NameValueCollection();
                var result = webClient.UploadValues(authUri, "POST", nameValues);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}