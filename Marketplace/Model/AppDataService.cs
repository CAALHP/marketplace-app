﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using caalhp.Core.Utils.Helpers;
using Newtonsoft.Json;
using MarketplaceContract;

namespace Marketplace.Model
{
    public class AppDataService : AppDataServiceBase, IAppDataService
    {
        public Task<IList<Application>> LoadApps(CancellationToken cancellationToken)
        {
            return LoadAppsAsync(cancellationToken);
        }

        public async Task<IList<Application>> LoadAppsAsync(CancellationToken cancellationToken)
        {
            return await Task.Run(() => GetAppInfos(), cancellationToken);
        }

        private IList<Application> GetAppInfos()
        {
            //TODO: get this url from config 
            const string relativeUrl = "api/applications";
            try
            {
                var webClient = new CookieWebClient();
                AuthorizeCaalhp(webClient);
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var uri = new Uri(BaseAddress, relativeUrl);
                var content = webClient.DownloadString(uri);
                var result = JsonConvert.DeserializeObject<IList<Application>>(content);
                return result;
            }
            catch (Exception exception)
            {
                return new List<Application>();
            }
        }
    }
}