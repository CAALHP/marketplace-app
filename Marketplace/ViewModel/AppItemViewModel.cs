﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using caalhp.Core.Events;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Marketplace.Model;
using System.Threading;
using Marketplace.Properties;
using caalhp.Core.Utils.Helpers;
using System.Globalization;
using MarketplaceContract;

namespace Marketplace.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AppItemViewModel : ViewModelBase
    {
        private bool _isDownloading;
        private bool _isInstalling;
        private bool _isInstalled;
        private int _downloadProgress;
        private readonly Application _appItem;
        private ApplicationProfile _appProfile;
        private string _imagePath;
        private readonly string _imageFolder;
        private bool _installFailed;
        private string _installingText;
        private string _installFailedText;

        /// <summary>
        /// Initializes a new instance of the AppItemViewModel class.
        /// </summary>
        public AppItemViewModel(Application appItem)
        {
            _appItem = appItem;
            _imageFolder = Path.Combine(AssemblyDirectory, "./Images/");
            IsDownloading = false;
            IsInstalling = false;
            IsInstalled = false;
            InitCulture();
            InstallingText = Resources.ResourceManager.GetString("Installing");
            InstallFailedText = Resources.ResourceManager.GetString("InstallFailed");
            ClickCommand = new RelayCommand(ClickCommand_Execute);
            //Messenger.Default.Register<NotificationMessage>(this, NotificationReceived);
            Messenger.Default.Register<DownloadAppCompletedEvent>(this, DownloadAppCompleted);
            Messenger.Default.Register<DownloadProgressEvent>(this, DownloadProgressChanged);
            Messenger.Default.Register<InstallAppCompletedEvent>(this, InstallAppCompleted);
            Messenger.Default.Register<InstallAppFailedEvent>(this, InstallAppFailed);
            Task.Run(() => LoadImage());

            //description and images are in a seperate profile
            Task.Run(() => LoadAppProfile());
        }

        private async Task LoadAppProfile()
        {
            await Task.Run(() => DownloadAppProfile());
        }

        private void DownloadAppProfile()
        {
            var dataService = new AppProfileDataService();
            AppProfile = dataService.LoadAppProfile(_appItem.ApplicationId, new CancellationToken()).Result;
        }

        private void InitCulture()
        {
            try
            {
                //get default culture
                var culture = CaalhpConfigHelper.GetDefaultCulture();
                //set language
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.Message);
            }
        }

        private async Task LoadImage()
        {
            await Task.Run(() => DownloadIcon(_appItem.Icon));
        }

        private void DownloadIcon(string icon)
        {
            var web = new WebClient();
            var fileName = Path.GetFileName(icon);
            if (fileName == null) return;
            var imagePath = Path.Combine(_imageFolder, fileName);
            if (!File.Exists(imagePath))
                web.DownloadFile(icon, Path.Combine(_imageFolder, imagePath));
            ImagePath = imagePath;
        }

        static private string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public bool IsDownloading
        {
            get { return _isDownloading; }
            private set
            {
                _isDownloading = value;
                RaisePropertyChanged();
            }
        }

        public bool IsInstalling
        {
            get { return _isInstalling; }
            private set
            {
                _isInstalling = value;
                RaisePropertyChanged();
            }
        }

        public bool InstallFailed
        {
            get { return _installFailed; }
            private set
            {
                _installFailed = value;
                RaisePropertyChanged();
            }
        }

        public bool IsInstalled
        {
            get { return _isInstalled; }
            set
            {
                _isInstalled = value;
                RaisePropertyChanged();
            }
        }

        public string AppName
        {
            get { return _appItem.Name; }
        }

        public int DownloadProgress
        {
            get { return _downloadProgress; }
            private set
            {
                _downloadProgress = value;
                RaisePropertyChanged();
            }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                _imagePath = value; RaisePropertyChanged();
            }
        }

        public string InstallingText
        {
            get { return _installingText; }
            set
            {
                _installingText = value;
                RaisePropertyChanged();
            }
        }

        public string InstallFailedText
        {
            get { return _installFailedText; }
            set
            {
                _installFailedText = value;
                RaisePropertyChanged();
            }
        }

        private void InstallAppFailed(InstallAppFailedEvent obj)
        {
            if (!obj.AppFileName.Equals(Path.GetFileName(_appItem.Url))) return;
            IsInstalling = false;
            IsInstalled = false;
            InstallFailed = true;
        }

        private void InstallAppCompleted(InstallAppCompletedEvent obj)
        {
            if (!obj.AppName.Equals(_appItem.Name)) return;
            IsInstalling = false;
            IsInstalled = true;
            InstallFailed = false;
        }

        private void DownloadProgressChanged(DownloadProgressEvent obj)
        {
            if (!obj.FileName.Equals(_appItem.Url)) return;
            DownloadProgress = obj.PercentDone;
        }

        private void DownloadAppCompleted(DownloadAppCompletedEvent obj)
        {
            if (!obj.AppName.Equals(_appItem.Name)) return;
            IsDownloading = false;
            IsInstalling = true;
        }

        #region ClickCommand

        public ICommand ClickCommand { get; private set; }

        public ApplicationProfile AppProfile
        {
            get { return _appProfile; }
            set
            {
                _appProfile = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public string Description
        {
            get
            {
                return _appProfile != null ? _appProfile.Description.Replace('<','[').Replace('>',']') : "";
            }
        }


        private void ClickCommand_Execute()
        {
            Messenger.Default.Send(new NotificationMessage<Application>(this, typeof(MarketplaceImplementation), _appItem, _appItem.Name));
            IsDownloading = true;
        }

        #endregion
    }
}