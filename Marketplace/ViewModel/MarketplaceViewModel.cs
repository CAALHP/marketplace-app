﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using caalhp.IcePluginAdapters;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using Marketplace.Model;
using Marketplace.Views;
using Marketplace.Properties;
using caalhp.Core.Utils.Helpers;
using System.Globalization;

namespace Marketplace.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MarketplaceViewModel : ViewModelBase
    {
        private readonly IAppDataService _appDataService;
        private MarketplaceImplementation _imp;
        private AppAdapter _adapter;

        private ObservableCollection<AppItemViewModel> _appItems;
        private string _loading;
        private string _welcomeText;

        public ObservableCollection<AppItemViewModel> AppItems
        {
            get { return _appItems; }
            set
            {
                _appItems = value;
                RaisePropertyChanged();
            }
        }

        public string Loading
        {
            get { return _loading; }
            set
            {
                _loading = value;
                RaisePropertyChanged();
            }
        }

        public string WelcomeText
        {
            get { return _welcomeText; }
            set
            {
                _welcomeText = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<string> InstalledApps { get; set; }

        /// <summary>
        /// Initializes a new instance of the MarketplaceViewModel class.
        /// </summary>
        public MarketplaceViewModel(IAppDataService appDataService)
        {
            InitCulture();
            WelcomeText = Resources.ResourceManager.GetString("Welcome");
            _appDataService = appDataService;
            AppItems = new ObservableCollection<AppItemViewModel>();
            InstalledApps = new ObservableCollection<string>();
            ConnectToCaalhp();
        }

        private void InitCulture()
        {
            try
            {
                //get default culture
                var culture = CaalhpConfigHelper.GetDefaultCulture();
                //set language
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.Message);
            }
        }

        private void ConnectToCaalhp()
        {
            const string endpoint = "localhost";
            try
            {
                _imp = new MarketplaceImplementation(this);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.Message);
            }
        }

        private async void LoadAppNames()
        {
            Loading = "Loading...";
            //var appNames = await _dataService.LoadAppListAsync(CancellationToken.None);
            var appNames = await _appDataService.LoadAppsAsync(CancellationToken.None);
            var items = new ObservableCollection<AppItemViewModel>();

            foreach (var item in appNames.Select(theApp => new AppItemViewModel(theApp) { IsInstalled = InstalledApps.Contains(theApp.Name) }))
            {
                items.Add(item);
            }
            DispatcherHelper.CheckBeginInvokeOnUI(() => { AppItems = items; });
            Loading = "";
        }

        public void Show()
        {
            LoadAppNames();
            Messenger.Default.Send(new NotificationMessage(this, typeof(MarketplaceView), "Show"));
        }
    }
}